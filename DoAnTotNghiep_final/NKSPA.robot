*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${BROWSER}                Chrome
${URL}                    http://45.117.81.50:5000/
${USER NAME}              admin
${PASSWORD}               Vbpo@12345
${PRODUCT_NAME}           Test Product 
${PRODUCT_DESCRIPTION}    This is a test product      

#-------------------PRODUCT-------------------
*** Test Cases ***
Check Product Name Field
    [Documentation]     Product-3   Check the mandatoryity of the product name input field in the "Add New" function
    Open Browser        ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     ${USERNAME}
    Input Password      id=signupSrPassword     ${PASSWORD}
    Click Button        xpath=//button[@type='submit']
    Wait Until Page Contains Element        xpath=//h1[contains(text(),'Trang ch')] 
    Sleep       3
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]    
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui
    Input Text     xpath=//input[@id='inputDosage']    1
    Input Text     xpath=//input[@id='inputKeyword']    VIP
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputProduct
    ${required}     Get Element Attribute       id=inputProduct     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputProduct" is required
    Sleep       3
    Close Browser

Check Product Price Field Required
    [Documentation]         Product-5   Compulsory Check Enter product price in function "Add New" Check the input feature
    LOGIN
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputFunction']    Khu mui
    Input Text     xpath=//input[@id='inputDosage']    1
    Input Text     xpath=//input[@id='inputKeyword']    VIP
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputPrice
    ${required}     Get Element Attribute       id=inputPrice     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputPrice" is required
    Sleep       3
    Close Browser

Check Product Functionality
    [Documentation]         Product-9   Check the mandatory field of the product use box in the "Add New" function
    LOGIN
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputDosage']    1
    Input Text     xpath=//input[@id='inputKeyword']    VIP
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputFunction
    ${required}     Get Element Attribute       id=inputFunction     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputFunction" is required
    Sleep       3
    Close Browser

Check Frequency Field Required
    [Documentation]     Product-10   Compulsory Check of the frequency of use box
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    
    Input Text     xpath=//input[@id='inputKeyword']    VIP
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputFunction
    ${required}     Get Element Attribute       id=inputDosage     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputDosage" is required
    Sleep       3
    Close Browser

Check Product Keyword
    [Documentation]     Product-11    Kiểm tra tính bắt buộc của ô từ khoá "Routine" trong chức năng "Thêm mới"
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    
    Input Text     xpath=//input[@id='inputDosage']    1           
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputKeyword
    ${required}     Get Element Attribute       id=inputKeyword     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputKeyword" is required
    Sleep       3
    Close Browser

Check Required Estimator Field
    [Documentation]     Product-12	"Kiểm tra tính bắt buộc của ô ước lượng sử dụng" trong chức năng "Thêm mới"
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    
    Input Text     xpath=//input[@id='inputDosage']    1   
    Input Text     xpath=//input[@id='inputKeyword']    VIP        
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputEstimate
    ${required}     Get Element Attribute       id=inputEstimate     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputEstimate" is required
    Sleep       3
    Close Browser

Check The Obligatory Field Of Note
    [Documentation]     Product-13	"Kiểm tra tính bắt buộc của ô Lưu ý" trong chức năng "Thêm mới"
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    
    Input Text     xpath=//input[@id='inputDosage']    1   
    Input Text     xpath=//input[@id='inputKeyword']    VIP        
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Click Button    xpath=//button[@type='submit']
    Element Should Be Visible       id=inputNote
    ${required}     Get Element Attribute       id=inputNote     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputNote" is required
    Sleep       3
    Close Browser

Check Function Add Image Product
    [Documentation]     Product-14	Kiểm tra tính năng "Thêm hình ảnh sản phẩm"
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    
    Input Text     xpath=//input[@id='inputDosage']    1   
    Input Text     xpath=//input[@id='inputKeyword']    VIP        
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Choose File     //*[@id="FormFileProduct"]      D:/ImgCreator.ai .png
    Sleep    3
    Click Element    //button[@type='submit']
    Wait Until Page Contains Element        //*[@id="datatable"]/tbody/tr[1]/td[9]/a/img  
    Sleep    3
    Close Browser 

Check Required Add Product Image
    [Documentation]     Product-15	Kiểm tra tính bắt buộc của chức năng thêm hình ảnh sản phẩm trong chức năng "Thêm mới"
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element    xpath=//a[@href='/manage/add/product']
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]  
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa
    Input Text     xpath=//input[@id='inputPrice']    100000
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    
    Input Text     xpath=//input[@id='inputDosage']    1   
    Input Text     xpath=//input[@id='inputKeyword']    VIP        
    Input Text     xpath=//input[@id='inputEstimate']    1 it
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi
    Click Button    xpath=//button[@type='submit']   
    Sleep       4
    ${file_input}    Get WebElement    id="FormFileProduct"
    ${file_name}    Get Element Attribute    ${file_input}    value
    Run Keyword If    '${file_name}' == ''    Fail    "Bạn cần chọn một tệp để thêm hình ảnh sản phẩm"
    Sleep       3
    Close Browser

Check Research Product Results
    [Documentation]     Product-16	"Kiểm tra tính năng Kết quả tìm kiếm sản phẩm
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Click Element       //*[@id="datatableSearch"]
    Input Text      //*[@id="datatableSearch"]      Nuoc Hoa
    Sleep   3
    Wait Until Page Contains Element    //*[@id="datatable"]/tbody/tr/td[2]
    Sleep   3
    Close Browser 

Check Function Add Product
    [Documentation]    Product-17	"Kiểm tra chức năng thêm mới sản phẩm"
    LOGIN         # Đăng nhập vô website với tài khoản admin
    Click Element                       xpath=//a[@href='/manage/product']        # Click vào chức năng Sản phẩm
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']        
    Click Element    xpath=//a[@href='/manage/add/product']        # Click vào chức năng thêm mới sản phẩm
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Thêm mới sản phẩm')]        # Vui lòng đợi element này xuất hiện
    Sleep       3
    Input Text     //*[@id="inputProduct"]     Nuoc Hoa             # Nhập vào trường tên sản phẩm
    Input Text     xpath=//input[@id='inputPrice']    100000        # Nhập vào trường giá sản phẩm
    Input Text     xpath=//input[@id='inputFunction']    Khu mui    # Nhập vào trường công dụng sản phẩm
    Input Text     xpath=//input[@id='inputDosage']    1            # Nhập vào trường tần suất sử dụng
    Input Text     xpath=//input[@id='inputKeyword']    VIP         # Nhập vào trường từ khoá Routine
    Input Text     xpath=//input[@id='inputEstimate']    1 it       # Nhập vào trường ước lượng sử dụng
    Input Text     //*[@id="inputNote"]    Tranh anh nang mat troi  # Nhập vào trường lưu ý
    Sleep    3
    Click Button    xpath=//button[@type='submit']        # Click vào nút Lưu
    Sleep       4        # Dừng 4 giây để hiển thị phần tử ở trên
    Wait Until Page Contains Element    //*[@id="content"]/div[1]/div[1]/div/div[1]/h1        # Vui lòng đợi cho element này xuất hiện
    Element Should Be Visible   //*[@id="datatable"]/tbody/tr[1]        # Chỉ định phần tử đó xuất hiện trên trang
    Close Browser        # Đóng trình duyệt kết thúc quá trình kiểm thử

Check Function Product Modify
    [Documentation]     Product-19	Kiểm tra chức năng chỉnh sửa sản phẩm
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Sleep   2
    Click Element       //*[@id="datatable"]/tbody/tr[1]/td[10]/a[1]
    Wait Until Page Contains Element    //*[@id="content"]/div[1]/div[1]/div/div/h1
    Clear Element Text      //*[@id="inputPrice"]
    Input Text      //*[@id="inputPrice"]       1200000
    Sleep   3
    Click Button   //*[@id="content"]/div[1]/div[2]/form/div[2]/button
    Element Should Be Visible   //*[@id="datatable"]/tbody/tr[1]/td[3]
    Sleep   3
    Close Browser

Check Function Delete Product
    [Documentation]     Product-20	 Kiểm tra chức năng xoá sản phẩm
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Sleep   2
    Click Element   //*[@id="datatable"]/tbody/tr[1]/td[10]/a[2]
    Wait Until Page Contains Element    //*[@id="DeleteProduct"]/div/div
    Sleep   3
    Click Element       //*[@id="DeleteProductForm"]/div[7]/div/button[2]
    ${alert_present} =    Alert Should Be Present
    Run Keyword If    ${alert_present}    Handle Alert    else    Log    
    Sleep   3
    Close Browser
      
Check Field Disabled When Delete Product
    [Documentation]     Product-21	Kiểm tra tính năng vô hiệu hoá các trường khi xoá thông tin sản phẩm
    LOGIN 
    Click Element                       xpath=//a[@href='/manage/product']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/product']
    Sleep   2
    Click Element   //*[@id="datatable"]/tbody/tr[1]/td[10]/a[2]
    Wait Until Page Contains Element    //*[@id="DeleteProduct"]/div/div
    Sleep   3
    Click Element   //*[@id="inputDeleteNote"]
    Element Should Be Disabled      //*[@id="inputDeleteNote"]
    Sleep   3
    Close Browser

#-------------------SERVICE-------------------
Check Required Service Name Field
    [Documentation]     Service-3	Kiểm tra tính bắt buộc của trường "Tên dịch vụ" trong chức năng "Thêm mới dịch vụ"
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep       3
    Click Element       //*[@id="content"]/div[1]/div[1]/div/div[2]  
    Input Text      //*[@id="inputFunction"]    Chong nang
    Input Text      //*[@id="inputNote"]        Tranh nuoc   
    Sleep       3
    Click Button    //*[@id="ServiceForm"]/div[6]/div/button[2]
    Wait Until Page Contains Element    xpath=//div[@id='ShowMessage'][contains(text(), 'Vui lòng điền vào ô bắt buộc')]
    Sleep       3
    Close Browser

Check Input Name Service 
    [Documentation]     Service-4	Kiểm tra tính năng "Nhập" trùng tên dịch vụ
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep       3
    Click Element       //*[@id="content"]/div[1]/div[1]/div/div[2]
    Input Text      //*[@id="inputService"]     Combo A1  
    Input Text      //*[@id="inputFunction"]    Chong nang
    Input Text      //*[@id="inputNote"]        Tranh nuoc   
    Sleep       3
    Click Button    //*[@id="ServiceForm"]/div[6]/div/button[2]
    Page Should Contain Element    //*[@id="ShowMessage"]  
    Sleep       5
    Close Browser

Check Required Function
    [Documentation]     Service-5	Kiểm tra tính bắt buộc của ô "Tác dụng"
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep       3
    Click Element       //*[@id="content"]/div[1]/div[1]/div/div[2]
    Input Text      //*[@id="inputService"]     CCCCCCCCCC 
    Input Text      //*[@id="inputNote"]        Tranh nuoc   
    Sleep       3
    Click Button    //*[@id="ServiceForm"]/div[6]/div/button[2]   
    Wait Until Page Contains Element    xpath=//div[@id='ShowMessage'][contains(text(), 'Vui lòng điền vào ô bắt buộc')]
    Sleep       3
    Close Browser

Check Required Note
    [Documentation]     Service-6	Kiểm tra tính bắt buộc của ô "Lưu ý"
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep       3
    Click Element       //*[@id="content"]/div[1]/div[1]/div/div[2]
    Input Text      //*[@id="inputService"]     CCCCCCCCCC  
    Input Text      //*[@id="inputFunction"]    Chong nang     
    Sleep       3
    Click Button    //*[@id="ServiceForm"]/div[6]/div/button[2]   
    Wait Until Page Contains Element    xpath=//div[@id='ShowMessage'][contains(text(), 'Vui lòng điền vào ô bắt buộc')]
    Sleep       3
    Close Browser

Check Function Add Service
    [Documentation]     Service-7	Kiểm tra chức năng thêm mới dịch vụ
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep       3
    Click Element       //*[@id="content"]/div[1]/div[1]/div/div[2]
    Input Text      //*[@id="inputService"]     CCCCCaaaaa  
    Input Text      //*[@id="inputFunction"]    Chong nang
    Input Text      //*[@id="inputNote"]        Tranh nuoc   
    Sleep       3
    Click Button    //*[@id="ServiceForm"]/div[6]/div/button[2]
    Sleep       3
    Wait Until Page Contains Element        //*[@id="content"]/div[1]/div[1]/div/div[1]/h1
    Element Should Be Visible     //*[@id="datatable"]/tbody/tr[1]/td[2]
    Sleep       5
    Close Browser

Check Function Modify Service
    [Documentation]     Service-8	Kiểm tra chức năng chỉnh sửa dịch vụ
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep       3
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[5]/a[1]
    Input Text      //*[@id="inputEditService"]     CAOSU  
    Input Text      //*[@id="inputEditFunction"]    Chong nang
    Input Text      //*[@id="inputEditNote"]        Tranh nuoc
    Sleep   5
    Click Button      //*[@id="ServiceEditForm"]/div[6]/div/button[2]
    Wait Until Page Contains Element        //*[@id="content"]/div[1]/div[1]/div/div[1]/h1
    Element Should Be Visible       //*[@id="datatable"]/tbody/tr[1]
    Sleep   5
    Close Browser

Check Function Delete Service
    [Documentation]     Service-9	Kiểm tra chức năng xoá dịch vụ
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep   3
    Click Element   //*[@id="datatable"]/tbody/tr[1]/td[5]/a[2]
    Click Button    //*[@id="ServiceDeleteForm"]/div[6]/div/button[2]
    ${alert_present} =    Alert Should Be Present
    Run Keyword If    ${alert_present}    Handle Alert    else    Log   
    Sleep   5
    Close Browser

Check Service Infor Search Results
    [Documentation]     Service-10	Kiểm tra tính năng "Kết quả" tìm kiếm thông tin dịch vụ
    LOGIN 
    Click Element       xpath=//a[@href='/manage/service']
    Wait Until Page Contains Element     xpath=//a[@href='/manage/service']
    Sleep   3
    Click Element   //*[@id="datatableSearch"]
    Input Text      //*[@id="datatableSearch"]      Combo A1
    Wait Until Element Is Visible       //*[@id="datatable"]/tbody
    Sleep   5
    Close Browser      

#-------------------USER-------------------
Check Required Of Username
    [Documentation]     User-2	Kiểm tra tính bắt buộc của Username trong chức năng thêm mới User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPassword2"]       Hoainhu2911@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Wait Until Element Is Visible    xpath=//div[contains(@class, 'alert-danger') and contains(text(), 'Vui lòng điền vào ô bắt buộc!')]
    Close Browser

Check The Same Feature Username
    [Documentation]     User-3	Kiểm tra tính năng nhập username có cho phép trùng với username khác không khi "Thêm mới User"
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        adc
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPassword2"]       Hoainhu29110365606152@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Wait Until Element Is Visible    xpath=//div[contains(@class, 'alert-danger') and contains(text(), 'Username này đã tồn tại.')]
    Close Browser

Check Required Password
    [Documentation]    User-4	Kiểm tra tính bắt buộc của ô Password trong chức năng thêm mới User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        adc
    Input Text      //*[@id="inputPassword2"]       Hoainhu2911@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Element Should Be Visible       //*[@id="ShowMessage"]
    Close Browser

Check Password Confirm Required
    [Documentation]     User-7	Kiểm tra tính bắt buộc của ô xác nhận lại mật khẩu
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        adc
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Element Should Be Visible       //*[@id="ShowMessage"]
    Close Browser

Check Phone Required
    [Documentation]     User-10	Kiểm tra tính bắt buộc của ô SĐT trong chức năng thêm mới User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        adc
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPassword2"]       Hoainhu2911@
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Element Should Be Visible       //*[@id="ShowMessage"]
    Close Browser

Check Input Duplicate Phone
    [Documentation]     User-12	Kiểm tra tính năng nhập trùng SĐT trong chức năng thêm mới User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        adc
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPassword2"]       Hoainhu2911@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Element Should Be Visible       //*[@id="ShowMessage"]
    Close Browser

Check Email Address Duplicate
    [Documentation]     User-14  Kiểm tra địa chỉ email trùng với email đã sử dụng trong chức năng thêm mới User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        adc
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPassword2"]       Hoainhu2911@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Element Should Be Visible   //*[@id="ShowMessage"]
    Close Browser    

Check Function Add User
    [Documentation]     User-17	 Kiểm tra chức năng thêm mới User  
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="content"]/div[1]/div[1]/div/div[2]/a
    Input Text      //*[@id="inputUsername"]        aaaa
    Input Text      //*[@id="inputPassword1"]       Hoainhu2911@
    Input Text      //*[@id="inputPassword2"]       Hoainhu2911@
    Input Text      //*[@id="inputPhone"]           0365606152
    Input Text      //*[@id="inputEmail"]           nhudoo0301@gmail.com
    Input Text      //*[@id="inputAddress"]         23 my an 4
    Sleep   3
    Click Button    //*[@id="UserForm"]/div[9]/div/button[2]
    Sleep   3
    Wait Until Page Contains Element    //*[@id="ShowMessageSuccess"]
    Sleep   3
    Element Should Be Visible       //*[@id="datatable"]/tbody/tr[1]
    Close Browser

Check Function User Modify
    [Documentation]     User-18	Kiểm tra chức năng chỉnh sửa User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element       //*[@id="datatable"]/tbody/tr[1]/td[6]/a[1]
    Clear Element Text     id=inputEditEmail
    Sleep   3
    Input Text      id=inputEditEmail       nhudoo0301@gmail.com
    Sleep   3
    Click Button       //*[@id="UserEdiForm"]/div[7]/div/button[2]
    Wait Until Page Contains Element    //*[@id="ShowMessageSuccess"]
    Sleep   3
    Element Should Be Visible       //*[@id="datatable"]/tbody/tr[1]
    Sleep   3
    Close Browser

Check Function Research Infor User
    [Documentation]     User-19	  Kiểm tra chức năng tìm kiếm thông tin User
    LOGIN 
    Click Element   xpath=//a[@href='/manage/user']
    Sleep   3
    Wait Until Page Contains Element       xpath=//a[@href='/manage/user']
    Click Element   //*[@id="datatableSearch"]
    Input Text      //*[@id="datatableSearch"]      sdssds
    Wait Until Page Contains Element    //*[@id="datatable"]/tbody/tr/td[2]
    Sleep   3
    Close Browser

*** Keywords ***
LOGIN 
    Open Browser        ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     ${USERNAME}
    Input Password      id=signupSrPassword     ${PASSWORD}
    Click Button        xpath=//button[@type='submit']
    Wait Until Page Contains Element        xpath=//h1[contains(text(),'Trang ch')] 
    Sleep       3

Input Password
    [Arguments]    ${locator}    ${password}
    Input Text     ${locator}    ${password}    mask=True