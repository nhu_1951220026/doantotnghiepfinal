** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}   http://45.117.81.50:5000/ 
#TÀI KHOẢN
${ID_USERNAME}    signupSrUsername
${ID_PASSWORD}    signupSrPassword    
${USER NAME}    admin
${PASSWORD}    Vbpo@12345
${NAME}    NHU
#KHÁCH HÀNG

#ID THÊM MỚI KHÁCH HÀNG
${ID_NAME}    inputFullname                             #ID TÊN KHÁCH HÀNG 
${ID_PHONE}    inputPhone                               #ID SỐ ĐIỆN THOẠI 
${ID_EMAIL}    inputEmail                               #ID EMAIL
${ID_AGE}    inputAge                                   #ID TUỔI
${ID_ADDR}    inputAddress                              #ID ĐỊA CHỈ
${ID_NN}    inputProfession                             #ID NGHỀ NGHIỆP
${ID_TT}    //*[@id="inputStatus"]                      #ID TÌNH TRẠNG
${ID_NKH}    //*[@id="inputType"]                       #ID NHÓM KHÁCH HÀNG            
${ID_NOTE}    inputNote                                 #ID GHI CHÚ
#ID CẬP NHẬT KHÁCH HÀNG
${IDU_NAME}    inputEditFullname                        #ID TÊN KHÁCH HÀNG 
${IDU_PHONE}    inputEditPhone                          #ID SỐ ĐIỆN THOẠI 
${IDU_EMAIL}    inputEditEmail                          #ID EMAIL
${IDU_AGE}    inputEditAge                              #ID TUỔI
${IDU_ADDR}    inputEditAddress                         #ID ĐỊA CHỈ
${IDU_NN}    inputEditProession                         #ID NGHỀ NGHIỆP
${IDU_TT}    //*[@id="inputEditStatus"]                 #ID TÌNH TRẠNG
${IDU_NKH}    //*[@id="inputEditType"]                  #ID NHÓM KHÁCH HÀNG            
${IDU_NOTE}    inputEditNote                            #ID GHI CHÚ


#ĐƠN KÊ_THÊM MỚI ĐƠN KÊ
${ID_TENKHACHHANG}    //*[@id="inputCustomer-tomselected"]                       #ID Tên khách hàng
${ID_STT}    //*[@id="datatableProduct"]/tbody/tr/td[1]/input                    #ID SỐ THỨ TỰ BẢNG CHI TIẾT THUỐC
${ID_NGAY}    //*[@id="datatableProduct"]/tbody/tr/td[2]/div                     #ID Ngày trong bảng chi tiết thuốc
${ID_TENSANPHAM}    //*[@id="datatableProduct"]/tbody/tr/td[3]/div/div/div[1]    #ID Tên sản phâm trong bảng chi tiết thuốc
${ID_BUOI}    //*[@id="datatableProduct"]/tbody/tr/td[4]/div/div/div[1]          #ID BUỔI TRONG CHI TIẾT THUỐC
${ID_XOAHANG}    //*[@id="datatableProduct"]/tbody/tr/td[5]/a                    #ID XOÁ HÀNG TRONG BẢNG CHI TIẾT THUỐC
${ID_THEMHANG}    //*[@id="btnProduct"]                                          #ID THÊM HÀNG TRONG BẢNG CHI TIẾT THUỐC
${ID_TENDICHVU}    //*[@id="datatableService"]/tbody/tr/td[1]/div/div/div[1]     #ID TÊN DỊCH VỤ BẢNG CHI TIẾT DỊCH VỤ
${ID_THOIGIAN}    //*[@id="datatableService"]/tbody/tr/td[2]/input               #ID THỜI GIAN
${ID_GHICHUDICHVU}    //*[@id="datatableService"]/tbody/tr/td[3]/input           #ID GHÍ CHÚ BẢNG DỊCH VỤ
${ID_XOAHANG_CHITIETDICHVU}    //*[@id="datatableService"]/tbody/tr/td[4]/a      #ID THAO TÁC XOÁ HÀNG CHI TIẾT DỊCH VỤ
${ID_THEMHANG_CHITIETDICHVU}    //*[@id="btnService"]                            #ID THAO TÁC THÊM HÀNG BẢNG CHI TIẾT DỊCH VỤ
${ID_NGAYTAIKHAM}    //*[@id="inputDayCalender"]/input                           #ID NGÀY TÁI KHÁM 
${ID_CACHDUNG}    //*[@id="inputUseto"]                                          #ID CÁCH DÙNG
${ID_CHITIETLICHKHAM}    //*[@id="inputDetailCalender"]                          #ID CHI TIẾT LỊCH KHÁM
${ID_ANHTRUOCDT}    //*[@id="FormFileBefore"]                                    #ID ẢNH TRƯỚC ĐIỀU TRỊ
${ID_ANHSAUDT}    //*[@id="FormFileAfter"]                                       #ID ẢNH SAU ĐIỀU TRỊ
${ID_GHICHU}    //*[@id="inputNote"]                                             #ID ghi chú đơn kê
${ID_BENHLY}    //*[@id="inputBenhLy"]                                           #ID BỆNH LÝ

# KEY ĐƠN KÊ
${ID_LUU_CAPNHATDONKE}    //*[@id="content"]/div[1]/div[2]/form/div[5]/button     
${ID_TIMKIEMDONKE}    //*[@id="datatableSearch"]
#ĐƠN KÊ_CẬP NHẬT ĐƠN KÊ
${IDU_TENKHACHHANG}    //*[@id="inputCustomer-tomselected"]                       #ID Tên khách hàng
${IDU_STT}    //*[@id="datatableProduct"]/tbody/tr/td[1]/input                    #ID SỐ THỨ TỰ BẢNG CHI TIẾT THUỐC
${IDU_NGAY}    //*[@id="datatableProduct"]/tbody/tr/td[2]/div/div/div[1]          #ID Ngày trong bảng chi tiết thuốc
${IDU_TENSANPHAM}    //*[@id="datatableProduct"]/tbody/tr/td[3]/div/div/div[1]    #ID Tên sản phâm trong bảng chi tiết thuốc
${IDU_BUOI}    //*[@id="datatableProduct"]/tbody/tr/td[4]/div/div/div[1]          #ID BUỔI TRONG CHI TIẾT THUỐC
${IDU_XOAHANG}    //*[@id="datatableProduct"]/tbody/tr/td[5]/a                    #ID XOÁ HÀNG TRONG BẢNG CHI TIẾT THUỐC
${IDU_THEMHANG}    //*[@id="btnProduct"]                                          #ID THÊM HÀNG TRONG BẢNG CHI TIẾT THUỐC
${IDU_TENDICHVU}    //*[@id="datatableService"]/tbody/tr/td[1]/div                #ID TÊN DỊCH VỤ BẢNG CHI TIẾT DỊCH VỤ
${IDU_THOIGIAN}    //*[@id="datatableService"]/tbody/tr/td[2]/input               #ID THỜI GIAN
${IDU_GHICHUDICHVU}    //*[@id="datatableService"]/tbody/tr/td[3]/input           #ID GHÍ CHÚ BẢNG DỊCH VỤ
${IDU_XOAHANG_CHITIETDICHVU}    //*[@id="datatableService"]/tbody/tr/td[4]/a      #ID THAO TÁC XOÁ HÀNG CHI TIẾT DỊCH VỤ
${IDU_THEMHANG_CHITIETDICHVU}    //*[@id="btnService"]                            #ID THAO TÁC THÊM HÀNG BẢNG CHI TIẾT DỊCH VỤ
${IDU_NGAYTAIKHAM}    //*[@id="inputDayCalender"]/input                           #ID NGÀY TÁI KHÁM 
${IDU_CACHDUNG}    //*[@id="inputUseto"]                                          #ID CÁCH DÙNG
${IDU_CHITIETLICHKHAM}    //*[@id="inputDetailCalender"]                          #ID CHI TIẾT LỊCH KHÁM
${IDU_ANHTRUOCDT}   //*[@id="FormFileBefore"]                                     #ID ẢNH TRƯỚC ĐIỀU TRỊ
${IDU_ANHSAUDT}    //*[@id="FormFileAfter"]                                       #ID ẢNH SAU ĐIỀU TRỊ
${IDU_GHICHU}    //*[@id="inputEditNote"]                                         #ID ghi chú đơn kê
${IDU_BENHLY}    //*[@id="inputEditBenhLy"]                                       #ID BỆNH LÝU


*** Keywords ***
LOGIN
    Open Browser    ${URL}    googlechrome
    Maximize Browser Window
    Input Text    ${ID_USERNAME}    ${USER NAME}
    sleep    2s
    Input Text    ${ID_PASSWORD}    ${PASSWORD}
    sleep    2s
    Click Button    Đăng nhập
CUSTOMER_ADD_OPEN
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/customer']
    sleep    2s
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]
    Sleep    2s 
OPEN_UPDATE_CUSTOMER
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')] 
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/customer']
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[11]/a[1]
    Sleep    2s
PRESCRIPTION_OPEN
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/order']
   
# KEY THÊM MỚI KHÁCH HÀNG
Add_customer_name
    [Arguments]    ${name}    
    Input Text    ${ID_NAME}    ${name} 
Add_customer_phone
    [Arguments]    ${phone}    
    Input Text    ${ID_PHONE}    ${phone}
Add_customer_age
    [Arguments]     ${age}          
    Input Text    ${ID_AGE}    ${age}
Add_customer_address
    [Arguments]    ${address}           
    Input Text    ${ID_ADDR}    ${address}
Add_customer_email
    [Arguments]        ${email}         
    Input Text    ${ID_EMAIL}    ${email}
Add_customer_Nghenghiep
    [Arguments]    ${Nghenghiep}     
    Input Text    ${ID_NN}    ${Nghenghiep}
Add_customer_Tinhtrang
    [Arguments]    ${TINHTRANG}   
    Select From List by Index  ${ID_TT}     ${TINHTRANG}
Add_customer_NHOMKHACHHANG
    [Arguments]    ${NHOMKHACHHANG}      
    Select From List by Index  ${ID_NKH}    ${NHOMKHACHHANG}
Add_customer_NOTE
    [Arguments]   ${NOTE}        
    Input Text    ${ID_NOTE}    ${NOTE} 



#KEY CẬP NHẬT KHÁCH HÀNG
Up_customer_name
    [Arguments]   ${name1}    
    Input Text    ${IDU_NAME}    ${name1} 
Up_customer_phone
    [Arguments]    ${phone1}    
    Input Text    ${IDU_PHONE}    ${phone1}
Up_customer_age
    [Arguments]     ${age1}          
    Input Text    ${IDU_AGE}    ${age1}
Up_customer_address
    [Arguments]    ${address1}           
    Input Text    ${IDU_ADDR}    ${address1}
Up_customer_email
    [Arguments]        ${email1}         
    Input Text    ${IDU_EMAIL}    ${email1}
Up_customer_Nghenghiep
    [Arguments]    ${Nghenghiep1}     
    Input Text    ${IDU_NN}    ${Nghenghiep1}
Up_customer_Tinhtrang
    [Arguments]    ${TINHTRANG1}   
    Select From List by Index  ${IDU_TT}     ${TINHTRANG1}
Up_customer_NHOMKHACHHANG
    [Arguments]    ${NHOMKHACHHANG1}      
    Select From List by Index  ${IDU_NKH}    ${NHOMKHACHHANG1}
Up_customer_NOTE
    [Arguments]   ${NOTE1}        
    Input Text    ${IDU_NOTE}    ${NOTE1} 




# KEY THÊM MỚI ĐƠN KÊ
Add_PRE_NAME 
    Click Element    ${ID_TENKHACHHANG}     
    Wait Until Element Is Visible   ${ID_TENKHACHHANG}  
    Wait Until Element Is Visible   inputCustomer-opt-10   timeout=20s
    Execute JavaScript   document.getElementById('inputCustomer-opt-10').click()
Add_PRE_STT 
    [Arguments]    ${stt}
    Input Text    ${ID_STT}   ${stt} 
Add_PRE_DAY
    Click Element    ${ID_NGAY}     
    Wait Until Element Is Visible   id=tomselect-2-opt-1   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-2-opt-1').click()
Add_PRE_TENSANPHAM
    Click Element    ${ID_TENSANPHAM}     
    Wait Until Element Is Visible   id=tomselect-3-opt-1   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-3-opt-1').click()
Add_PRE_BUOI
    Click Element    ${ID_BUOI}     
    Wait Until Element Is Visible   id=tomselect-4-opt-1   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-4-opt-1').click()  
Add_PRE_XOAHANG
    Click Element     ${ID_XOAHANG}        
Add_PRE_THEMHANG
    Click Element     ${ID_THEMHANG}       
Add_PRE_TENDICHVU
    Click Element    ${ID_TENDICHVU}     
    Wait Until Element Is Visible   id=tomselect-5-opt-1   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-5-opt-1').click()  
Add_PRE_THOIGIAN
    [Arguments]    ${TIME}
    Input Text    ${ID_THOIGIAN}    ${TIME}
Add_PRE_GHICHUDICHVU
     [Arguments]    ${GCDV}
    Input Text     ${ID_GHICHUDICHVU}    ${GCDV}
Add_PRE_NGAYTAIKHAM
    Click Element    ${ID_NGAYTAIKHAM}     
    Wait Until Element Is Visible   xpath=//span[@class="flatpickr-day " and @aria-label="May 10, 2023"]   timeout=10s
    Click Element   xpath=//span[@class="flatpickr-day " and @aria-label="May 10, 2023"]
Add_PRE_CACHDUNG
    [Arguments]   ${CD}
    Input Text    ${ID_CACHDUNG}    ${CD}
Add_PRE_CHITIETLICHKHAM
    [Arguments]   ${CTLK}
    Input Text    ${ID_CHITIETLICHKHAM}    ${CTLK}
Add_PRE_ANHTRUOCDIEUTRI
    Choose File    ${ID_ANHTRUOCDT}    \C\Users\ADMIN\Desktop\Demo-main\key\path\a.jpg
Add_PRE_ANHTRUOCSAUTRI
    Choose File    ${ID_ANHSAUDT}   \C\Users\ADMIN\Desktop\Demo-main\key\path\a.jpg
Add_PRE_GHICHU
     [Arguments]   ${GC}
    Input Text    ${ID_GHICHU}    ${GC}
Add_PRE_BENHLY
    [Arguments]   ${BL}
    Input Text    ${ID_BENHLY}    ${BL}

#KEY CẬP NHẬT ĐƠN KÊ
Update_PRE_STT 
    [Arguments]    ${stt}
    Input Text    ${IDU_STT}   ${stt} 
Update_PRE_DAY
    Click Element    ${IDU_NGAY}     
    Wait Until Element Is Visible   id=tomselect-2-opt-5   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-2-opt-5').click()
Update_PRE_TENSANPHAM
    Click Element    ${IDU_TENSANPHAM}     
    Wait Until Element Is Visible   id=tomselect-3-opt-1   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-3-opt-1').click()
Update_PRE_BUOI
    Click Element    ${IDU_BUOI}     
    Wait Until Element Is Visible   id=tomselect-4-opt-1   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-4-opt-1').click()  
Update_PRE_XOAHANG
    Click Element     ${IDU_XOAHANG}     
Update_PRE_THEMHANG
    Click Element     ${IDU_THEMHANG}    
Update_PRE_TENDICHVU
    Click Element    ${IDU_TENDICHVU}     
    Wait Until Element Is Visible   id=tomselect-5-opt-4   timeout=10s
    Execute JavaScript   document.getElementById('tomselect-5-opt-4').click()  
Update_PRE_THOIGIAN
    [Arguments]    ${TIME}
    Input Text    ${IDU_THOIGIAN}    ${TIME}
Update_PRE_GHICHUDICHVU
     [Arguments]    ${GCDV}
    Input Text     ${IDU_GHICHUDICHVU}    ${GCDV}
Update_PRE_NGAYTAIKHAM
    Click Element    ${IDU_NGAYTAIKHAM}     
    Wait Until Element Is Visible   xpath=//span[@class="flatpickr-day " and @aria-label="May 10, 2023"]   timeout=10s
    Click Element   xpath=//span[@class="flatpickr-day " and @aria-label="May 10, 2023"]
Update_PRE_CACHDUNG
    [Arguments]   ${CD}
    Input Text    ${IDU_CACHDUNG}    ${CD}
Update_PRE_CHITIETLICHKHAM
    [Arguments]   ${CTLK}
    Input Text    ${IDU_CHITIETLICHKHAM}    ${CTLK}
Update_PRE_ANHTRUOCDIEUTRI
    Choose File    ${IDU_ANHTRUOCDT}    \C\Users\ADMIN\Desktop\Demo-main\key\path\a.jpg
Update_PRE_ANHTRUOCSAUTRI
    Choose File    ${IDU_ANHSAUDT}   \C\Users\ADMIN\Desktop\Demo-main\key\path\a.jpg
Update_PRE_GHICHU
     [Arguments]   ${GC}
    Input Text    ${IDU_GHICHU}    ${GC}
Update_PRE_BENHLY
    [Arguments]   ${BL}
    Input Text    ${IDU_BENHLY}    ${BL}



