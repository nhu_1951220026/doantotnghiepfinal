*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${BROWSER}                Google Chrome
${URL}                    http://45.117.81.50:5000/
${USER NAME}              admin
${PASSWORD}               Vbpo@12345
${PRODUCT_NAME}           Test Product 
${PRODUCT_DESCRIPTION}    This is a test product       
*** Keywords ***
LOGIN
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text        id=signupSrUsername     ${USERNAME}
    Input Password    id=signupSrPassword    ${PASSWORD}
    Click Button      xpath=//button[@type='submit']
    Sleep       3
LOGIN_1
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text        id=signupSrUsername     ${USERNAME}
    Input Password    id=signupSrPassword    ${PASSWORD}
    Click Button      xpath=//button[@type='submit']
    Sleep       3
CHANGE
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        ${PASSWORD}
    Input Text        xpath=//input[@id='resetPasswordnew']     Vbpo@12345
    Input Text        xpath=//input[@id='resetPasswordrepeat']  Vbpo@12345
    CLick Button      xpath=//button[@type='submit']
*** Test Cases ***
login_1 
    [Documentation]     Login-4	Đăng nhập với user và Password rỗng	
    Open Browser       ${URL}      ${BROWSER}
    maximize browser window
    Click Button      xpath=//button[@type='submit']
    Sleep       3
    Element Should Be Visible       id=signupSrUsername
    ${required}     Get Element Attribute       id=signupSrUsername     required
    Run Keyword If  '${required}' == 'true'  Log  The field "signupSrUsername" is required
    Sleep       3
    Close Browser
login_2
    [Documentation]       Login-5 Đăng nhập khi không nhập user
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     ${EMPTY}
    Input Password      id=signupSrPassword     ${PASSWORD}
    Click Button        xpath=//button[@type='submit']
    Sleep       3
    Element Should Be Visible       id=signupSrUsername
    ${required}     Get Element Attribute       id=signupSrUsername     required
    Run Keyword If  '${required}' == 'true'  Log  The field "signupSrUsername" is required
    Close Browser
login_3
    [Documentation]     Login-6 	Đăng nhập khi không nhập Password
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     ${USERNAME}
    Input Password      id=signupSrPassword     ${EMPTY}
    Click Button        xpath=//button[@type='submit']
    Sleep       3
    Element Should Be Visible       id=signupSrPassword
    ${required}     Get Element Attribute       id=signupSrPassword     required
    Run Keyword If  '${required}' == 'true'  Log  The field "signupSrPassword" is required
    Close Browser 
login_4
    [Documentation]     Login-7	    Đăng nhập với user hợp lệ và Password không hợp lệ
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     ${USER NAME}
    Input Password      id=signupSrPassword     Vbpo12344
    Click Button        xpath=//button[@type='submit']
    Sleep   3
    Wait Until Element Is Visible   //*[@id="content"]/div/div/div[2]/div/div/span
    Element Text Should Be  //*[@id="content"]/div/div/div[2]/div/div/span      Thông tin tài khoản hoặc mật khẩu không đúng !! 
    Close Browser
login_5
    [Documentation]     Login-8	    Đăng nhập với user không hợp lệ và Password hợp lệ
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     nhu
    Input Password      id=signupSrPassword     ${PASSWORD}
    Click Button        xpath=//button[@type='submit']
    Sleep   3
    Wait Until Element Is Visible   //*[@id="content"]/div/div/div[2]/div/div/span
    Element Text Should Be  //*[@id="content"]/div/div/div[2]/div/div/span      Thông tin tài khoản hoặc mật khẩu không đúng !! 
    Close Browser
login_6
    [Documentation]     Login-9	    Đăng nhập với độ dài Password phù hợp ( 8 ký tự trở lên )
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text          id=signupSrUsername     nhu
    Input Password      id=signupSrPassword     nhu123
    Click Button        xpath=//button[@type='submit']
    Sleep   3
    Element Should Be Visible       id=signupSrPassword
    ${required}     Get Element Attribute       id=signupSrPassword     required
    Run Keyword If  '${required}' == 'true'  Log  The field "signupSrPassword" is required
login_7
    [Documentation]     Login-11	Đăng nhập với user và Password đúng
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text        id=signupSrUsername     ${USERNAME}
    Input Password    id=signupSrPassword    ${PASSWORD}
    Click Button      xpath=//button[@type='submit']
    Sleep       3
    Close Browser
login_8
    [Documentation]     Login-16	Đăng nhập một lúc nhiều tab, nhiều trình duyệt cùng 1 tài khoản
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text        id=signupSrUsername     ${USERNAME}
    Input Password    id=signupSrPassword    ${PASSWORD}
    Click Button      xpath=//button[@type='submit']
    Sleep       3
    Open Browser      ${URL}      ${BROWSER}
    maximize browser window
    Input Text        id=signupSrUsername     ${USERNAME}
    Input Password    id=signupSrPassword    ${PASSWORD}
    Click Button      xpath=//button[@type='submit']
    Sleep       3
    Wait Until Page Contains Element        //*[@id="content"]/div[1]/div[1]/div/div/h1
    Close Browser
*** Test Case ***
change_1
    [Documentation]     Change Password -2	Nhập đúng Password cũ và thay đổi Password mới
    LOGIN
    CHANGE
    Sleep      3
    Wait Until Page Contains    Mật khẩu đã được thay đổi thành công    timeout=5s
    CLose Browser
change_2
    [Documentation]     Change Password -3	Nhập sai Password cũ
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        Vbpo@1234
    Input Text        xpath=//input[@id='resetPasswordnew']     Vbpo@12345
    Input Text        xpath=//input[@id='resetPasswordrepeat']  Vbpo@12345
    CLick Button      xpath=//button[@type='submit']
    Element Text Should Be  //*[@id="content"]/div/div/div[2]/div/div      Mật khẩu cũ chưa chính xác!
    Sleep       3
change_3
    [Documentation]     Change Password -4	Nhập đúng Password cũ
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        Nhu@123
    Input Text        xpath=//input[@id='resetPasswordnew']     Vbpo@12345
    Input Text        xpath=//input[@id='resetPasswordrepeat']  Vbpo@12345
    CLick Button      xpath=//button[@type='submit']
    Element Text Should Be      //*[@id="content"]/div/div/div[2]/div/div      Mật khẩu đã được thay đổi thành công
    Sleep       3
change_4
    [Documentation]     Change Password -5	Không nhập Password cũ
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        ${EMPTY}
    Input Text        xpath=//input[@id='resetPasswordnew']     Vbpo@12345
    Input Text        xpath=//input[@id='resetPasswordrepeat']  Vbpo@12345
    CLick Button      xpath=//button[@type='submit']
    Element Should Be Visible       id=inputPassword
    ${required}     Get Element Attribute       id=inputPassword     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputPassword" is required
    Sleep       3
change_5
    [Documentation]     Change Password -6	Không nhập Password mới
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        Vbpo@12345
    Input Text        xpath=//input[@id='resetPasswordnew']     ${EMPTY}
    Input Text        xpath=//input[@id='resetPasswordrepeat']  ${EMPTY}
    CLick Button      xpath=//button[@type='submit']
    Element Should Be Visible       id=resetPasswordnew
    ${required}     Get Element Attribute       id=resetPasswordnew     required
    Run Keyword If  '${required}' == 'true'  Log  The field "resetPasswordnew" is required
    Sleep       3
change_6
    [Documentation]     Change Password -7	Để trống cả ba trường hợp
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        ${EMPTY}
    Input Text        xpath=//input[@id='resetPasswordnew']     ${EMPTY}
    Input Text        xpath=//input[@id='resetPasswordrepeat']  ${EMPTY}
    CLick Button      xpath=//button[@type='submit']
    Element Should Be Visible       id=inputPassword    
    ${required}     Get Element Attribute       id=inputPassword     required
    Run Keyword If  '${required}' == 'true'  Log  The field "inputPassword" is required
    Sleep       3
change_7
    #Testcase fail
    [Documentation]     Change Password -12	Kiểm tra Password cũ trùng Password mới
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        ${PASSWORD}
    Input Text        xpath=//input[@id='resetPasswordnew']     ${PASSWORD}
    Input Text        xpath=//input[@id='resetPasswordrepeat']  ${PASSWORD}
    CLick Button      xpath=//button[@type='submit']
    Element Should Be Visible       id=inputPassword    
    Sleep   3
    Wait Until Page Contains    Mật khẩu cũ trùng với mật khẩu mới    timeout=5s
    Close Browser
change_8
    [Documentation]     Change Password -13	Kiểm tra Password cũ sai
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        Nhunhu@123
    Input Text        xpath=//input[@id='resetPasswordnew']     ${PASSWORD}
    Input Text        xpath=//input[@id='resetPasswordrepeat']  ${PASSWORD}
    CLick Button      xpath=//button[@type='submit']
    Element Should Be Visible       id=inputPassword    
    Sleep   3
    Element Text Should Be      //*[@id="content"]/div/div/div[2]/div/div      Mật khẩu cũ chưa chính xác!
    Close Browser
change_9
    [Documentation]     Change Password -14	Kiểm tra các ký tự đặc biệt
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element     xpath=//a[@href='/changepassword']
    Sleep   3
    Input Text        xpath=//input[@id='inputPassword']        Nhunhu@123
    Input Text        xpath=//input[@id='resetPasswordnew']     ${PASSWORD}
    Input Text        xpath=//input[@id='resetPasswordrepeat']  ${PASSWORD}
    CLick Button      xpath=//button[@type='submit']
    Element Should Be Visible       id=inputPassword    
    Sleep   3
    Element Text Should Be      //*[@id="content"]/div/div/div[2]/div/div/strong      Mật khẩu phải có 8 kí tự gồm chữ hoa, thường, số, kí tự đặc biệt!
    Close Browser
*** Test Case ***
logout_1
    [Documentation]     Logout-1	Kiểm tra chức năng đăng xuất
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element       //*[@id="header"]/div/div[2]/ul/li[4]/div/div/a[2]
    Sleep   3
    Close Browser
logout_3
    [Documentation]     Kiểm tra phiên đăng Nhập
    Open Browser          ${URL}      ${BROWSER}    alias=window1
    maximize browser window
    Input Text        id=signupSrUsername     ${USERNAME}
    Input Password    id=signupSrPassword    ${PASSWORD}
    Click Button      xpath=//button[@type='submit']
    Sleep       3
    LOGIN
    Click Element    id=accountNavbarDropdown
    Wait Until Element Is Visible    id=accountNavbarDropdown
    Sleep   3
    CLick Element    //*[@id="header"]/div/div[2]/ul/li[4]/div/div/a[2]
    Sleep   3
    Close Window
    Switch Browser    window1
    Wait Until Page Contains Element   //*[@id="content"]/div[1]/div[1]/div/div/h1
    Sleep    2
    Reload Page
    Sleep    2
    [Teardown]    Close All Browsers
# # Add Customer
#     [Tags]      customer
#     Open Browser      ${URL}      ${BROWSER}
#     maximize browser window
#     Input Text          id=signupSrUsername     ${USER NAME}
#     Input Password      id=signupSrPassword     ${PASSWORD}
#     Click Button        xpath=//button[@type='submit']
#     Sleep       3
#     CLick Element       xpath=//a[@href='/manage/customer']
#     Sleep       3
#     Click Element    //a[contains(@class, 'btn-primary') and contains(@data-bs-target, 'AddCustomer')]
#     Sleep       3
#     Input Text          id=inputFullname    Hiiiiiii
#     Input Text          id=inputPhone       091234567122
#     Input Text          id=inputEmail       bn4v@gmail.com   
#     Input Text          id=inputAge         22
#     Input Text          id=inputAddress     Quang Tri
#     Input Text          id=inputProfession  Sinh vien
#     Select From List By Label   id=inputStatus  Đã dùng
#     Select From List By Label   id=inputType    Trẻ em
#     Sleep       3
#     Click Button        //*[@id="profile"]/form/div[12]/div/button[2]      
#     Sleep   3
#     Input Text          id=datatableSearch       Hiiiiiii
#     Sleep       3


# Input Password 
    # [Arguments]    ${locator}    ${password}
    # Input Text    ${locator}    ${password}    mask=True