
** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem
Library    String
Resource   Pres_Key.robot 
*** Variables ***
${kitudatbiet}    #$%^&*@#$%^&
${Null}
${EXPECTED_ROWS}    4
${statusCustomer1}    ""
*** Test Cases ***
# TEST CASE CHỨC NĂNG KHÁCH HÀNG

_01
    #Thêm khách hàng với "Nhập" tất cả thông tin hợp lệ
    [Documentation]    Customer-2    Add new guest row with input full information
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}      
    Add_customer_email   nhudoo0301@GMAI.COM
    Add_customer_age    20
    Add_customer_address    QA  
    Add_customer_Nghenghiep    QC
    Add_customer_Tinhtrang    1
    Add_customer_NHOMKHACHHANG    1
    Add_customer_NOTE    ABC
    Click Button    Lưu
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_02
    #"Chức Năng Thêm khách hàng:Kiểm tra tính năng bắt buộc trường Khách hàng"
    [Documentation]  Customer-3    Add Customer Function:Check the Customer field required feature
    CUSTOMER_ADD_OPEN
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number} 
    Add_customer_email     HOAINHUCT2@GMAI.COM
    Add_customer_age    20
    Add_customer_address    QA  
    Add_customer_Nghenghiep    QC
    Add_customer_Tinhtrang    1
    Add_customer_NHOMKHACHHANG    1
    Add_customer_NOTE    ABC
    Click Button    Lưu
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]   Vui lòng điền vào ô bắt buộc!      
    Close Browser
_03
    #"Chức Năng Thêm:Kiểm tra tính bắt buộc trường SĐT"
    [Documentation]  Customer-4       "Additional Function: Check the obligatory phone number field"
    CUSTOMER_ADD_OPEN
    Add_customer_name    hoainhu
    Add_customer_email     nhudoo0301@GMAI.COM
    Add_customer_age    20
    Add_customer_address    QA  
    Add_customer_Nghenghiep    QC
    Add_customer_Tinhtrang    1
    Add_customer_NHOMKHACHHANG    1
    Add_customer_NOTE    ABC
    Click Button    Lưu
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    Vui lòng điền vào ô bắt buộc!
    Close Browser
_04 
    #Thêm khách hàng với chỉ nhập 2 trường bắt buộc là Tên đăng nhập và SĐT
    [Documentation]    Customer-5    Add a customer by entering only 2 required fields: Username and Phone number
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}         
    Click Button    Lưu
    sleep    3s
    Input Text    //*[@id="datatableSearch"]    ${random_number}  
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_05
    #"Thêm khách hàng với chỉ nhập 2 trường bắt buộc là Tên đăng nhập và SĐT.Nhập SĐT không đúng với yêu cầu(không phải là số hoặc không trong phạm vi 10-13 số)."
    [Documentation]    Customer-6    Add a customer with only 2 required fields: Username and phone number. Enter the wrong phone number (not a number or not in the range of 10-13 numbers)."
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(10, 10000))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}         
    Click Button    Lưu
    sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    SĐT không đúng định dạng(10 - 13 số)
    Close Browser
_06
    #"Thêm khách hàng với chỉ nhập 2 trường bắt buộc là Tên đăng nhập và SĐT.Nhập SĐT đã tồn tại."
    [Documentation]    Customer-7    "Add a customer with only 2 required fields: Username and Phone number. Enter an existing phone number."
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    Add_customer_phone    2222222222222         
    Click Button    Lưu
    sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    SĐT này đã tồn tại trên hệ thống
    Close Browser
_07
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Email khách hàng với định dạng hợp lệ
    [Documentation]    Customer-8    Add customer by entering 2 required fields, Username and Phone number and request to add Customer Email with valid format
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_email     nhudoo0301@GMAI.COM
    sleep    3s
    Click Button    Lưu
    sleep    3s
    Input Text    //*[@id="datatableSearch"]    ${random_number}  
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_08
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và  yêu cầu thêm Email khách hàng với định dạng không hợp lệ
    [Documentation]    Customer-9    Add customer by entering 2 required fields, Username and Phone number and request to add Customer Email with invalid format
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_email    @@@
    Click Button    Lưu
    sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    Email này không đúng định dạng
    Close Browser
_09
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và  yêu cầu thêm Tuổi khách hàng với các trường hợp số tuổi phù hợp
    [Documentation]    Customer-10    Add customer with 2 required fields, Username and Phone number and request to add Customer Age with appropriate age cases
     CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}     
    Add_customer_age    20
    Click Button    Lưu
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_10 
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và  yêu cầu thêm Tuổi khách hàng với các trường hợp không phải số và ký tự đặt biệt
    [Documentation]    Customer-11    Add a customer with 2 required fields, Username and Phone number and request to add Customer Age with non-numeric and special characters
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}     
    Add_customer_age    -11
    Click Button    Lưu
    sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    CCCD/CMND, SĐT, Tuổi chỉ chứa kí tự số
    Close Browser
_11
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Địa chỉ khách hàng với các trường hợp hợp lệ
    [Documentation]    Customer-12    Add customer with 2 required fields: Username and Phone number and request to add Customer Address with valid cases
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_address    QA  
    sleep    3s
    Click Button    Lưu
    sleep    3s
    Input Text    //*[@id="datatableSearch"]    ${random_number}  
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_12
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Địa chỉ khách hàng với các trường hợp có thêm ký tự đặt biệt
    [Documentation]    Customer-13     Add a customer with 2 required fields: Username and Phone number and request additional Customer Address with special characters in cases   
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_address    ${kitudatbiet}  
    sleep    3s
    Click Button    Lưu
    sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    Yêu cầu nhập định dạng địa chỉ phù hợp
    Close Browser
_13
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Nghề nghiệp khách hàng với các trường hợp hợp lệ
    [Documentation]    Customer-14    Add a customer by entering 2 required fields, Username and Phone number and request additional Customer Occupation with valid cases
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_Nghenghiep    QC  
    sleep    3s
    Click Button    Lưu
    sleep    3s
    Input Text    //*[@id="datatableSearch"]    ${random_number}  
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_14
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Nghề nghiệp khách hàng với các trường hợp có ký tự đặt biệt
    [Documentation]    Customer-15    Add a customer with 2 required fields, Username and Phone number, and request additional Customer Occupation with special characters
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_Nghenghiep    ${kitudatbiet}    
    sleep    3s
    Click Button    Lưu
    sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    Yêu cầu nhập định dạng nghề nghiệp phù hợp  
    Close Browser 
_15
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Ghi chú khách hàng với các trường hợp hợp lệ
    [Documentation]    Customer-16    Add a customer with 2 required fields, Username and Phone number and request additional Customer Notes with valid cases
    CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    Add_customer_NOTE    ABC  
    sleep    3s
    Click Button    Lưu
    sleep    3s
    Input Text    //*[@id="datatableSearch"]    ${random_number}  
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_16
    #Thêm khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Ghi chú khách hàng với các trường hợp không hợp lệ(vượt quá số lượng quy định)
     [Documentation]    Customer-17    Add a customer with 2 required fields: Username and Phone number and request additional Customer Notes with invalid cases (exceeding the specified number)
     CUSTOMER_ADD_OPEN
    ${current_time}=  Get Time
    Add_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Add_customer_phone    ${random_number}
    ${random_string}=    Generate Random String    200    ascii_letters
    Log    ${random_string}
    Add_customer_NOTE     ${random_string}
    sleep    3s
    Click button    Lưu
    Sleep    3s
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    Yêu cầu nhập định dạng ghi chú phù hợp phù hợp_17
    Close Browser
   
_17
     #Thêm khách hàng với tất cả các trường để dữ liệu rỗng
    [Documentation]    Customer-18    Add customer with all fields to empty data
    CUSTOMER_ADD_OPEN
    Click Button    Lưu
    Wait Until Element Is Enabled   //*[@id="ShowMessage"] 
    Element Text Should Be    //*[@id="ShowMessage"]    Vui lòng điền vào ô bắt buộc!
    Close Browser


_20
    #Tìm kiếm khách hàng với định nghĩa trùng với thông tin khách hàng
    [Documentation]    Customer-21    Search for customers with the same definition as customer information
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/customer']
    Input Text    //*[@id="datatableSearch"]    2023-12-05 17:51:40  
    sleep    2s
    Element Text Should Be    //*[@id="datatable"]/tbody/tr/td[2]/a/div/h5    2023-12-05 17:51:40       
    Close Browser 
_21
    [Documentation]    Customer-22    Search for customers with a definition that does not match customer information
    #Tìm kiếm khách hàng với định nghĩa không trùng với thông tin khách hàng
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/customer']
    Input Text    //*[@id="datatableSearch"]    sde23f23f32f 
    sleep    2s
    Element Text Should Be    //*[@id="datatable"]/tbody/tr/td/div    Không có dữ liệu
    Close Browser

_23 
     #Hiện thị danh sách khách hàng theo số lượng được lựa chọn
    [Documentation]    Customer-24    Display a list of customers according to the selected quantity
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/customer']
    Click Element   xpath=//*[@id="datatableEntries"]/option[text()="${EXPECTED_ROWS}"]
    ${row_count}    Get Element Count    //*[@id="datatable"]/tbody/tr
    Log             ${row_count}        
    Sleep    5s
    Should Be Equal As Integers    ${row_count}    ${EXPECTED_ROWS}
    Close Browser
_25
    #Cập nhật khách hàng với "Nhập" tất cả thông tin hợp lệ
    [Documentation]    Customer-26    Update customer with "Enter" all valid information
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number}      
    Up_customer_email     HOAINHUCT2@GMAI.COM
    Up_customer_age    20
    Up_customer_address    QA  
    Up_customer_Nghenghiep    QC
    Up_customer_Tinhtrang    1
    Up_customer_NHOMKHACHHANG    1
    Up_customer_NOTE    ABC
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
   # Wait Until Page Contains Element     //*[@id="inputEditFullname"]
   # ${input_value}    Get Text     //*[@id="inputEditFullname"]
   # ${is_empty}       Run Keyword And Retur  n Status    Should Be Empty    ${input_value}
   # Run Keyword If    ${is_empty} == ${True}     Input Text     //*[@id="inputEditFullname"]    Your Input Text

    #Wait Until Page Contains Element     //*[@id="inputEditPhone"]
    #${input_value}    Get Text     //*[@id="inputEditPhone"]
    #${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    #Run Keyword If    ${is_empty} == ${True}     Input Text     //*[@id="inputEditPhone"]    123456
    
    #Wait Until Page Contains Element     //*[@id="inputEditEmail"]
    #${input_value}    Get Text     //*[@id="inputEditEmail"]
    #${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    #Run Keyword If    ${is_empty} == ${True}    Input Text     //*[@id="inputEditEmail"]    @@@@@
    Close Browser
_26
    #"Cập nhật khách hàng:Kiểm tra tính năng bắt buộc trường Tên Khách hàng"
     [Documentation]    Customer-27    "Update Customer:Check for Mandatory Customer Name Field"
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number}      
    Up_customer_email     HOAINHUCT2@GMAI.COM
    Up_customer_age    20
    Up_customer_address    QA  
    Up_customer_Nghenghiep    QC
    Up_customer_Tinhtrang    1
    Up_customer_NHOMKHACHHANG    1
    Up_customer_NOTE    ABC
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    Lỗi thiếu thông tin bắt buộc: Tên khách hàng
    Close Browser
_27
    #"Cập nhật khách hàng:Kiểm tra tính năng bắt buộc trường SĐT"
    [Documentation]    Customer-28    "Customer Update:Check the feature that requires the phone number field"
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}   
    Up_customer_email     HOAINHUCT2@GMAI.COM
    Up_customer_age    20
    Up_customer_address    QA  
    Up_customer_Nghenghiep    QC
    Up_customer_Tinhtrang    1
    Up_customer_NHOMKHACHHANG    1
    Up_customer_NOTE    ABC
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Element Text Should Be        //*[@id="ShowEditMessage"]    Lỗi thiếu thông tin bắt buộc: LỗiNOT NULL constraint failed: core_customer.numberphone
    Close Browser
_28
    #Cập nhật khách hàng với chỉ nhập 2 trường bắt buộc là Tên đăng nhập và SĐT
    [Documentation]    Customer-29    Update customers with only 2 required fields: Username and Phone number
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number}      
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_29
    #"Cập nhật khách hàng với chỉ nhập 2 trường bắt buộc là Tên đăng nhập và SĐT.Nhập SĐT không đúng với yêu cầu(không phải là số hoặc không trong phạm vi 10-13 số)."
    [Documentation]    Customer-30    "Update customer with only 2 required fields, Username and Phone number. Enter the wrong phone number (not a number or not within 10-13 numbers)."
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(10, 1000))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number}      
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    SĐT không đúng định dạng(10 - 13 số)
    Close Browser
_30
    #"Cập nhật khách hàng với chỉ nhập 2 trường bắt buộc là Tên đăng nhập và SĐT.Nhập SĐT đã tồn tại."
    [Documentation]    Customer-31    "Update customers with only 2 required fields: Username and Phone number.Enter an existing phone number."
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_phone    234576789974         
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    LỗiUNIQUE constraint failed: core_customer.numberphone
    Close Browser
_31
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Email khách hàng với định dạng hợp lệ
    [Documentation]    Customer-32    Update customer by entering 2 required fields, Username and Phone number and request to add Customer Email with valid format
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_email     HOAINHUCT2@GMAI.COM   
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_32
    #cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và  yêu cầu thêm Email khách hàng với định dạng không hợp lệ
    [Documentation]    Customer-33    Update customer with 2 required fields, Username and Phone number and request to add Customer Email with invalid format
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_email    @@@  
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    Email này không đúng định dạng    
    Close Browser
_33   
    #Chỉnh sửa khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và  yêu cầu thêm Tuổi khách hàng với các trường hợp số tuổi phù hợp
    [Documentation]    Customer-34    Edit customers by entering 2 required fields: Username and Phone number and request to add Customer Age with appropriate age cases
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number}      
    Up_customer_age    20
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
_34
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và  yêu cầu thêm Tuổi khách hàng với các trường hợp không phải số và ký tự đặt biệt
    [Documentation]    Customer-35    Update customer with 2 required fields, Username and Phone number, and request to add Customer Age with non-numeric and special characters
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number}      
    Up_customer_email     HOAINHUCT2@GMAI.COM
    Up_customer_age    -12
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    CCCD/CMND, SĐT, Tuổi chỉ chứa kí tự số   
    Close Browser
_35
    #cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Địa chỉ khách hàng với các trường hợp hợp lệ
    [Documentation]    Customer-36    Update customer with 2 required fields, Username and Phone number and request to add Customer Address with valid cases
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_address    QA
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_36
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Địa chỉ khách hàng với các trường hợp có thêm ký tự đặt biệt
    [Documentation]    Customer-37    Update customer by entering 2 required fields: Username and Phone number and request to add Customer Address with special characters in cases
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_address    @#$%^&
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    Địa chỉ này không đúng định dạng    
    Close Browser
_37
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Nghề nghiệp khách hàng với các trường hợp hợp lệ
    [Documentation]    Customer-38    Update customer by entering 2 required fields, Username and Phone number and request more Customer Occupation with valid cases
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_Nghenghiep    QC
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_38
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Nghề nghiệp khách hàng với các trường hợp có ký tự đặt biệt
    [Documentation]    Customer-39    Update customer by entering 2 required fields, Username and Phone number and request more Customer occupation with special characters
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_Nghenghiep    @#$%^&@#$%^&*
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    sleep    3s
    Element Text Should Be        //*[@id="ShowEditMessage"]    Nghề nghiệp này không đúng định dạng 
    Close Browser   
_39
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Ghi chú khách hàng với các trường hợp hợp lệ
    [Documentation]    Customer-40    Update customer by entering 2 required fields, Username and Phone number and request additional Customer Notes with valid cases
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    Up_customer_NOTE    ABC
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Input Text    //*[@id="datatableSearch"]    ${random_number} 
    sleep    2s
    Page Should Contain    ${current_time}
    Close Browser
_40
    #Cập nhật khách hàng với nhập 2 trường bắt buộc là Tên đăng nhập và SĐT và yêu cầu thêm Ghi chú khách hàng với các trường hợp không hợp lệvượt quá số lượng quy định)
    [Documentation]    Customer-41    Update customer by entering 2 required fields, Username and Phone number and request additional Customer Notes with invalid cases exceeding the specified number)
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Sleep    2s
    ${current_time}=  Get Time
    Up_customer_name    ${current_time}
    ${random_number}=    Run    python -c "import random; print(random.randint(1000000000, 9999999999))"
    Log    Random number is: ${random_number}
    Up_customer_phone    ${random_number} 
    ${random_string}=    Generate Random String    200    ascii_letters
    Log    ${random_string}
    Up_customer_NOTE    ${random_string}    
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Sleep    2s
    Element Text Should Be        //*[@id="ShowEditMessage"]    Ghi chú này không đúng định dạng    
    Close Browser
_41
    #Cập nhật khách hàng với tất cả các trường để dữ liệu rỗng
    [Documentation]    Customer-42    Update customer with all fields to empty data
    OPEN_UPDATE_CUSTOMER
    Clear Element Text     //*[@id="inputEditFullname"]
    Clear Element Text     //*[@id="inputEditPhone"]
    Clear Element Text    inputEditEmail
    Clear Element Text    inputEditAge
    Clear Element Text    inputEditAddress
    Clear Element Text    inputEditProession
    Up_customer_Tinhtrang    0
    Up_customer_NHOMKHACHHANG    0
    Clear Element Text     //*[@id="inputEditNote"]
    Click Button    //*[@id="EditCustomerForm"]/div[12]/div/button[2]
    Sleep    2s
    Wait Until Element Is Enabled       //*[@id="ShowEditMessage"]
    Sleep    2s
    Element Text Should Be        //*[@id="ShowEditMessage"]    Vui lòng thêm vào ô bắt buộc 
    Close Browser

_42
    [Documentation]    Customer-43
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    #lấy thông tin từ table
    ${nameCustomer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[2]
    ${SDTCustommer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[4]
    ${emailCustommer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[5]
    ${addressCustomer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[6]
    ${ageCustomer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[7]
    ${statusCustomer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[8]
    ${typeCustomer}=    Get Text    //*[@id="datatable"]/tbody/tr[1]/td[9]
    ${noteCustomer}=    Get Text    //*[@id="validationInvalidTextarea1"]
    #gắng giá trị cho trường status
    Run Keyword If    '${statusCustomer}' == 1
    ...    Set Variable    ${statusCustomer1}  'Đã dùng'
    ...    ELSE IF    '${statusCustomer}' == 2
    ...    Set Variable    ${statusCustomer1}    'Chưa dùng'
    ...    ELSE
    ...    Set Variable    ${statusCustomer1}    0  
    Log    ${statusCustomer1}    
    #Mở layout edit
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[11]/a[1]
    Sleep    2
    
    #lấy giá trị từ layout edit
    ${getNameCustomer}=    Get Value   ${IDU_NAME}
    ${getSDTCustommer}=    Get Value    ${IDU_PHONE}
    ${getEmailCustommer}=    Get Value    ${IDU_EMAIL}
    ${getAddressCustomer}=    Get Value    ${IDU_ADDR}
    ${getAgeCustomer}=    Get Value    ${IDU_AGE}
    ${getStatusCustomer}=    Get Value    //*[@id="inputEditStatus"]
    ${getTypeCustomer}=    Get Value    //*[@id="inputEditType"]
    ${getNoteCustomer}=    Get Value    ${IDU_NOTE}
    
    #so sánh thông tin
    Should Be Equal As Strings  ${nameCustomer}  ${getNameCustomer}
    Should Be Equal As Strings  ${SDTCustommer}  ${getSDTCustommer}
    Should Be Equal As Strings  ${emailCustommer}  ${getEmailCustommer}
    Should Be Equal As Strings  ${addressCustomer}  ${getAddressCustomer}
    Should Be Equal As Strings  ${ageCustomer}  ${getAgeCustomer}
    Should Be Equal As Strings  ${noteCustomer}  ${getNoteCustomer}

_43
    #Xoá Khách hàng
    [Documentation]    Customer-45    Delete Customer
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/customer']
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[11]/a[2]
    Sleep    2
    Wait Until Page Contains Element    //*[@id="DeleteCustomerForm"]/div[12]/div/button[2]
    Click Element    //*[@id="DeleteCustomerForm"]/div[12]/div/button[2]
    Handle Alert    action=ACCEPT 
    Element Should Not Be Visible    //*[@id="DeleteCustomerForm"]/div[12]/div/button[2]
    Close Browser
l





# TEST CASE CHỨC NĂNG ĐƠN KÊ
_44
    #Thêm Đơn kê với mọi trường để dữ liệu rỗng
    [Documentation]    Prescription-2
    PRESCRIPTION_OPEN 
    Click Button    ${ID_LUU_CAPNHATDONKE}
    ${test}=    Get Element Attribute    //*[@id="inputCustomer-tomselected"]     validationMessage  
    Log    ${test}
    Should Be Equal As Strings    ${test}    Please fill out this field.
    Close Browser
_45
    #Thêm Đơn kê với nhập tất cả các trường với thông tin hợp lệ
    [Documentation]    Prescription-3
    PRESCRIPTION_OPEN
    Sleep    2
    ${value1}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id ban đầu
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_STT    140 
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Add_PRE_TENDICHVU
    Add_PRE_THOIGIAN    11111
    Add_PRE_GHICHUDICHVU    ABC
    Add_PRE_NGAYTAIKHAM
    Add_PRE_CACHDUNG    ABCDC
    Add_PRE_CHITIETLICHKHAM    GOOD
    #Add_PRE_ANHTRUOCDIEUTRI
    #Add_PRE_ANHSAUDIEUTRI
    Add_PRE_GHICHU    ABC
    Add_PRE_BENHLY    ABC
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2    
    ${value2}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id sau khi thêm mới 
    Should Be True    ${value2} > ${value1}                                   # So sánh hai giá trị
    Close Browser
_46
    #Thêm Đơn kê với thiếu trường khách hàng
    [Documentation]    Prescription-4    Add Prescription with missing customer field
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_STT    140 
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Add_PRE_TENDICHVU
    Add_PRE_THOIGIAN    11111
    Add_PRE_GHICHUDICHVU    ABC
    Add_PRE_NGAYTAIKHAM
    Add_PRE_CACHDUNG    ABCDC
    Add_PRE_CHITIETLICHKHAM    GOOD
    #Add_PRE_ANHTRUOCDIEUTRI
    #Add_PRE_ANHSAUDIEUTRI
    Add_PRE_GHICHU    ABC
    Add_PRE_BENHLY    ABC
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    ${test}=    Get Element Attribute    //*[@id="inputCustomer-tomselected"]     validationMessage  
    Log    ${test}
    Should Be Equal As Strings    ${test}    Please fill out this field.
    Close Browser
_47
    #Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc có các hàng đã thêm được nhập đầy đủ thông tin
    [Documentation]    Prescription-6    Add Prescription Unit with customer and Drug Details table with added rows filled in with all information
    PRESCRIPTION_OPEN
    Sleep    2
    ${value1}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id ban đầu
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_STT    140 
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2    
    ${value2}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id sau khi thêm mới 
    Should Be True    ${value2} > ${value1}                                   # So sánh hai giá trị
    Close Browser
_48
    #"Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc:có các cột trong hàng nhập thiếu dữ liệu"
    [Documentation]    Prescription-7    "Add Customer Prescription and Drug Details table:there are columns in the input row that are missing data"
    #Trường hợp: Trường STT RỖNG
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2 
    ${test}=    Get Element Attribute    //*[@id="datatableProduct"]/tbody/tr/td[1]/input     validationMessage  
    Log    ${test}
    Should Be Equal As Strings    ${test}    Please fill out this field.
    Close Browser
_49
    #"Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc:có các cột trong hàng nhập thiếu dữ liệu"
    [Documentation]    Prescription-7    "Add Customer Prescription and Drug Details table:there are columns in the input row that are missing data"
    #Trường hợp: Trường NGÀY, TÊN SẢN PHẨM, BUỔI RỖNG
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2 
    ${test}=    Get Element Attribute    //*[@id="tomselect-2-tomselected"]     validationMessage  
    Log    ${test}
    Should Be Equal As Strings    ${test}    Please fill out this field.
    Close Browser
_50
    #ĐANG CÓ LỖI
    #"Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc:Kiểm tra yêu cầu khi nhập STT"
    [Documentation]    Prescription-8    "Add Customer Prescription and Drug Details table:Check requirements when entering STT"
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_STT    140
    ${TEST}=    Get Value    //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    Should Match Regexp    ${TEST}    /^-?d+.?d*$/    #Kiểm tra giá trị được nhập vào có đúng với biểu thức chính quy
    Close Browser
_51
    #"Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc:có các hàng đã được xoá(các trường còn lại dữ liệu rỗng)"  
    [Documentation]    Prescription-9    "Add Customer Prescription and Drug Details table:have deleted rows (remaining fields are empty)"
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_XOAHANG
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2 
    Page Should Contain    Please fill out this field.
    Close Browser
_52
    #Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc với nhiều hàng dữ liệu :nhập vào STT Trùng lặp.
    [Documentation]    Prescription-10   Add Prescription with customer and Drug Details table with multiple rows of data :enter Duplicate No.





   









     

    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_THEMHANG
    Add_PRE_STT    1
    Input Text    //*[@id="datatableProduct"]/tbody/tr[2]/td[1]/input    1
    ${a} =    Get Value   //*[@id="datatableProduct"]/tbody/tr[1]/td[1]/input    #STT ĐẦU TIÊN
    ${b} =    Get Value    //*[@id="datatableProduct"]/tbody/tr[2]/td[1]/input   #STT THỨ 2
    Should Be True    ${a} != ${b} 
    Close Browser
_53
    #Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc đạt yêu cầu(các hàng đã nhập đầy đủ dữ liệu) và bảng chi tiết dịch vụ được nhập đầy đủ thông tin
    [Documentation]    Prescription-12    Add Prescription with customer and Satisfactory Drug Detail table (rows have entered all data) and service detail table is fully entered

    PRESCRIPTION_OPEN
    Sleep    2
    ${value1}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id ban đầu
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_STT    140 
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Add_PRE_TENDICHVU
    Add_PRE_THOIGIAN    11111
    Add_PRE_GHICHUDICHVU    ABC
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2    
    ${value2}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id sau khi thêm mới 
    Should Be True    ${value2} > ${value1}                                   # So sánh hai giá trị
    Close Browser
_54
    #Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc đạt yêu cầu(các hàng đã  được thêm nhập đầy đủ dữ liệu ) và bảng chi tiết dịch vụ được để rỗng
     [Documentation]    Prescription-13    Add Prescription with customer and Satisfactory Drug Details table (the rows have been filled with data) and the service detail table is empty

    PRESCRIPTION_OPEN
    Sleep    2
    ${value1}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id ban đầu
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_STT    140 
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Add_PRE_NGAYTAIKHAM
    Add_PRE_CACHDUNG    ABCDC
    Add_PRE_CHITIETLICHKHAM    GOOD
    #Add_PRE_ANHTRUOCDIEUTRI
    #Add_PRE_ANHSAUDIEUTRI
    Add_PRE_GHICHU    ABC
    Add_PRE_BENHLY    ABC
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2    
    ${value2}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id sau khi thêm mới 
    Should Be True    ${value2} > ${value1}                                   # So sánh hai giá trị
    Close Browser
_55
    #Thêm Đơn kê với khách hàng và bảng Chi tiết thuốc đạt yêu cầu(các hàng được thêm đã nhập đầy đủ dữ liệu ) và bảng chi tiết dịch vụ nhập thiếu dữ liệu các cột 
    [Documentation]    Prescription-14    Add Prescription with customer and Satisfactory Drug Details table (the added rows have full data) and the import service detail table is missing columns
    #THIẾU DỮ LIỆU MỘT SỐ CỘT Ở BẢNG CHI TIẾT DỊCH VỤ
     PRESCRIPTION_OPEN
    Sleep    2
    ${value1}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id ban đầu
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME
    Add_PRE_STT    140 
    Add_PRE_DAY
    Add_PRE_TENSANPHAM
    Add_PRE_BUOI
    Add_PRE_GHICHUDICHVU    ABC
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Sleep    2    
    ${value2}=    Get Text    xpath=//*[@id="datatable"]/tbody/tr[1]/td[1]    # Lưu giá trị id sau khi thêm mới 
    Should Be True    ${value2} > ${value1}                                   # So sánh hai giá trị
    Close Browser
_56
    #Thêm Đơn kê với bảng chi tiết thuốc có các hàng đã được xoá, các trường còn lại nhập đầy đủ thông tin
    [Documentation]    Prescription-16    Add Prescription with drug details table with deleted rows, the remaining fields are filled with information
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="content"]/div[1]/div[1]/div/div[2]/a 
    Sleep    2s
    Add_PRE_NAME    
    Add_PRE_TENDICHVU
    Add_PRE_THOIGIAN    11111
    Add_PRE_GHICHUDICHVU    ABC
    Add_PRE_NGAYTAIKHAM
    Add_PRE_CACHDUNG    ABCDC
    Add_PRE_CHITIETLICHKHAM    GOOD
    #Add_PRE_ANHTRUOCDIEUTRI
    #Add_PRE_ANHSAUDIEUTRI
    Add_PRE_GHICHU    ABC
    Add_PRE_BENHLY    ABC
    Click Button    ${ID_LUU_CAPNHATDONKE} 
    Page Should Contain   Please fill out this field.
    Close Browser
_57
    #Kiểm tra chức năng chỉnh sửa đơn kê
    [Documentation]    Prescription-19    Check out the prescription editing function
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[11]/a[1]/i
    Sleep    2s
    Location Should Be    http://45.117.81.50:8080/manage/update/order/150
    Close Browser
_58
    #Cập nhật Đơn kê với nhập tất cả các trường với thông tin hợp lệ
    [Documentation]    Prescription-21    Update unit with all fields entry with valid information
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2

    Wait Until Page Contains Element     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${input_value}    Get Value     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_STT    120

    Wait Until Page Contains Element     ${IDU_NGAY}    
    ${input_value}    Get Value     ${IDU_NGAY}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_DAY

    Wait Until Page Contains Element     ${IDU_TENSANPHAM}    
    ${input_value}    Get Value     ${IDU_TENSANPHAM}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_TENSANPHAM

    Wait Until Page Contains Element     ${IDU_BUOI}   
    ${input_value}    Get Value     ${IDU_BUOI}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_BUOI

    Wait Until Page Contains Element     ${IDU_TENDICHVU}   
    ${input_value}    Get Value     ${IDU_TENDICHVU}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_TENDICHVU

    Wait Until Page Contains Element     ${IDU_THOIGIAN}   
    ${input_value}    Get Value     ${IDU_THOIGIAN}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_THOIGIAN    11111

    Wait Until Page Contains Element     ${IDU_GHICHUDICHVU}  
    ${input_value}    Get Value     ${IDU_GHICHUDICHVU}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_GHICHUDICHVU    ABC
    
    Wait Until Page Contains Element     ${IDU_NGAYTAIKHAM}
    ${input_value}    Get Value     ${IDU_NGAYTAIKHAM}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_NGAYTAIKHAM    
    
    Wait Until Page Contains Element     ${IDU_CACHDUNG}
    ${input_value}    Get Value     ${IDU_CACHDUNG}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_CACHDUNG    ABC

    Click Element    ${ID_LUU_CAPNHATDONKE}    

    Location Should Be    http://45.117.81.50:8080/manage/order
    Close Browser
_59
    #Cập nhật Đơn kê với khách hàng và bảng Chi tiết thuốc có các hàng đã thêm được nhập đầy đủ thông tin
    [Documentation]    Prescription-24    Update Prescription with customer and Drug Details table with added rows fully entered
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2

    Wait Until Page Contains Element     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${input_value}    Get Value     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_STT    120

    Wait Until Page Contains Element     ${IDU_NGAY}    
    ${input_value}    Get Value     ${IDU_NGAY}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_DAY

    Wait Until Page Contains Element     ${IDU_TENSANPHAM}    
    ${input_value}    Get Value     ${IDU_TENSANPHAM}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_TENSANPHAM

    Wait Until Page Contains Element     ${IDU_BUOI}   
    ${input_value}    Get Value     ${IDU_BUOI}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_BUOI
    
    Click Element    ${ID_LUU_CAPNHATDONKE}
    Location Should Be    http://45.117.81.50:8080/manage/order
    Close Browser
_60
    #"Cập nhật Đơn kê với khách hàng và bảng Chi tiết thuốc có:Các hàng đã được xoá"
    [Documentation]    Prescription-25 "Update Prescription with customer and Drug Detail table with:Rows have been deleted"
     PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2
    Click Element    ${IDU_XOAHANG}    
    sleep    2
    Click Element    ${ID_LUU_CAPNHATDONKE}
    Location Should Be    http://45.117.81.50:8080/manage/order
    Close Browser
_61
    #"Cập nhật Đơn kê với khách hàng và bảng Chi tiết thuốc có:Các hàng có các cột được nhập thiếu dữ liệu"
    [Documentation]    Prescription-26   "Update Prescription with Customer and Drug Details table with: Rows with columns entered with missing data" 
    PRESCRIPTION_OPEN    
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2
    Clear Element Text    ${IDU_STT}
    Click Element    ${ID_LUU_CAPNHATDONKE}
    Sleep    2 
    ${test}=    Get Element Attribute    //*[@id="datatableProduct"]/tbody/tr/td[1]/input     validationMessage  
    Log    ${test}
    Should Be Equal As Strings    ${test}    Please fill out this field.
    Close Browser
_62
    #Cập nhật Đơn kê với khách hàng và bảng Chi tiết thuốc với nhiều hàng dữ liệu :nhập vào STT Trùng lặp.
    [Documentation]    Prescription-27     Update Prescription with customer and Drug Details table with many rows of data : enter Duplicate No.
    PRESCRIPTION_OPEN    
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2
    Clear Element Text    ${IDU_STT}
    Click Element    //*[@id="btnProduct"]
    
    Input Text    ${IDU_STT}    1
    Input Text    //*[@id="datatableProduct"]/tbody/tr[2]/td[1]/input    1
    ${a} =    Get Value   ${IDU_STT}                                             #STT ĐẦU TIÊN
    ${b} =    Get Value    //*[@id="datatableProduct"]/tbody/tr[2]/td[1]/input   #STT THỨ 2
    Should Be True    ${a} != ${b} 
    Close Browser
_63
    #"Cập nhật Đơn kê :Bảng chi tiết dịch vụ được nhập đầy đủ thông tin"
    [Documentation]    Prescription-29   "Update Prescription : The service details table is fully entered"   
    PRESCRIPTION_OPEN  
      Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2

    Wait Until Page Contains Element     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${input_value}    Get Value     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_STT    120

    Wait Until Page Contains Element     ${IDU_NGAY}    
    ${input_value}    Get Value     ${IDU_NGAY}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_DAY

    Wait Until Page Contains Element     ${IDU_TENSANPHAM}    
    ${input_value}    Get Value     ${IDU_TENSANPHAM}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_TENSANPHAM

    Wait Until Page Contains Element     ${IDU_BUOI}   
    ${input_value}    Get Value     ${IDU_BUOI}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_BUOI

    Wait Until Page Contains Element     ${IDU_TENDICHVU}   
    ${input_value}    Get Value     ${IDU_TENDICHVU}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_TENDICHVU

    Wait Until Page Contains Element     ${IDU_THOIGIAN}   
    ${input_value}    Get Value     ${IDU_THOIGIAN}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_THOIGIAN    11111

    Wait Until Page Contains Element     ${IDU_GHICHUDICHVU}  
    ${input_value}    Get Value     ${IDU_GHICHUDICHVU}
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_GHICHUDICHVU    ABC
    
    
    Click Element    ${ID_LUU_CAPNHATDONKE}
    
    Location Should Be    http://45.117.81.50:8080/manage/order
    Close Browser
_64
    #Kiểm tra trang chi tiết đơn kê
    [Documentation]    Prescription-32    Check prescription details page
    PRESCRIPTION_OPEN  
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[2]/a/div/h5
    # Kiểm tra thuộc tính Disabled trong bảng chi tiết đơn kê
    @{fields}    Create List    //*[@id="inputDayCalender"]    //*[@id="inputUseto"]    //*[@id="inputDetailCalender"]    //*[@id="inputEditNote"]
    FOR    ${field}    IN    @{fields}
        Element Should Be Disabled    ${field}
    END
    Close Browser
_65
    #Tìm kiếm Đơn kê với định nghĩa trùng với thông tin đã có
    [Documentation]    Prescription-33   Search Prescriptions with definitions that match existing information
    PRESCRIPTION_OPEN 
    Input Text    ${ID_TIMKIEMDONKE}    Ngô Văn Sinh
    Sleep    1
    Page Should Contain    Ngô Văn Sinh
    Sleep    1
    Close Browser
_66
    #Tìm kiếm Đơn kê với định nghĩa không trùng với thông tin đã có
    [Documentation]    Prescription-34   Search Prescriptions with definitions that do not match existing information
    PRESCRIPTION_OPEN 
    Input Text    ${ID_TIMKIEMDONKE}    No information
    Sleep    1
    Page Should Contain    Không có dữ liệu  
    Sleep    1  
    Close Browser 
_67
    #ĐANG CÓ LỖI
    #Hiện thị danh Đơn kê  theo số lượng được lựa chọn
    [Documentation]    Prescription-36    Display list of Prescriptions according to the selected quantity
    LOGIN
    Wait Until Page Contains Element    xpath=//h1[contains(text(),'Trang chủ')]
    sleep    2s
    Click Element    xpath=//a[@href='/manage/order']
    Wait Until Page Contains Element    xpath=//a[@href='/manage/order']
    Click Element   xpath=//*[@id="datatableEntries"]/option[text()="${EXPECTED_ROWS}"]
    ${row_count}    Get Element Count    //*[@id="datatable"]/tbody/tr
    Log             ${row_count}        
    Sleep    5s
    Should Be Equal As Integers    ${row_count}    ${EXPECTED_ROWS}
    Close Browser
_68
    #ĐANG CÓ LỖI
    #"Cập nhật Đơn kê với khách hàng và bảng Chi tiết thuốc:Kiểm tra yêu cầu khi nhập STT"
    [Documentation]    Prescription-28    "Update Prescription with customer and Drug Details table: Check requirements when entering STT"
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[2]/td[11]/a[1]
    Sleep    2
    Wait Until Page Contains Element     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${input_value}    Get Value     //*[@id="datatableProduct"]/tbody/tr/td[1]/input
    ${is_empty}       Run Keyword And Return Status    Should Be Empty    ${input_value}
    Run Keyword If    ${is_empty} == ${True}     Update_PRE_STT    1
    ${TEST}=    Get Value    ${IDU_STT}
    Should Match Regexp    ${TEST}    /^-?\d+\.?\d*$/    #Kiểm tra giá trị được nhập vào có đúng với biểu thức chính quy
    Close Browser
_69
    #Xoá một đơn kê 
    [Documentation]    Prescription-39
    PRESCRIPTION_OPEN
    Sleep    2
    Click Element    //*[@id="datatable"]/tbody/tr[1]/td[11]/a[2]/i
    Wait Until Page Contains Element    //*[@id="DeleteOrderForm"]/div[7]/div/button[2]    
    Click Element    //*[@id="DeleteOrderForm"]/div[7]/div/button[2]
    Handle Alert    action=ACCEPT 
    Element Should Not Be Visible    //*[@id="DeleteOrderForm"]/div[7]/div/button[2]                  

    